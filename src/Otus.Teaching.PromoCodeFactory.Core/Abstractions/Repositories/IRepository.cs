﻿using Otus.Teaching.PromoCodeFactory.Core.Domain;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IRepository<T>
       where T : BaseEntity
    {
        bool AutoSaveChanges { get; set; }

        IQueryable<T> Items { get; }

        Task<IEnumerable<T>> GetAllAsync(CancellationToken cancel = default);
        IEnumerable<T> GetAll();

        Task<T?> GetByIdAsync(Guid id, CancellationToken cancel = default);
        T? GetById(Guid id);

        Guid Create(T item);
        Task<Guid> CreateAsync(T item, CancellationToken cancel = default);

        bool Update(T item);
        Task<bool> UpdateAsync(T item, CancellationToken cancel = default);

        bool Delete(Guid id);
        Task<bool> DeleteAsync(Guid id, CancellationToken cancel = default);

        bool SaveChanges();
    }
}