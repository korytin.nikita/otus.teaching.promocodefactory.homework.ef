﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement
{
    public class Preference
        :BaseEntity
    {
        public string Name { get; set; }
        public string? Description { get; set; }

        public virtual ICollection<PromoCode> PromoCodes { get; set; } = new HashSet<PromoCode>();

        public virtual ICollection<CustomerPreference> CustomerPreferences { get; set; } = new HashSet<CustomerPreference>();
    }
}