using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Otus.Teaching.PromoCodeFactory.DataAccess.Abstractions;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost
{
    public class Program
    {
        public async static Task Main(string[] args)
        {
            var host = CreateHostBuilder(args).Build();

            if (host.Services.GetRequiredService<IConfiguration>().GetSection("Database")["Initialize"] == "true")
                using (var scope = host.Services.CreateScope())
                    await scope.ServiceProvider.GetRequiredService<IDatabaseInitializer>().InitializeAsync();

            host.Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
            Host.CreateDefaultBuilder(args)
                .ConfigureWebHostDefaults(webBuilder => { webBuilder.UseStartup<Startup>(); });
    }
}