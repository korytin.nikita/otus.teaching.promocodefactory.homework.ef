using Mapster;
using MapsterMapper;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;

public static class Registrar
{
    public static IServiceCollection AddMappers(this IServiceCollection services)
    {
        var config = TypeAdapterConfig.GlobalSettings;
        config.Scan(Assembly.GetExecutingAssembly());
        config.RequireDestinationMemberSource = true;
        config.Default.PreserveReference(true);
        config.AddLocalConfig();

        return services
            .AddSingleton(config)
            .AddScoped<IMapper, ServiceMapper>();
    }
}