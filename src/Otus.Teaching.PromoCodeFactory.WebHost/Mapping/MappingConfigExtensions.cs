using Mapster;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System.Linq;

internal static class MappingConfigExtensions
{
    internal static TypeAdapterConfig AddLocalConfig(this TypeAdapterConfig config)
    {
        config.NewConfig<CreateCustomerRequest, Customer>()
            .IgnoreNullValues(true)
            .Map(
                i => i.CustomerPreferences, 
                i => i.PreferenceIds.Select(i => new CustomerPreference { PreferenceId = i}).ToList())
            .Ignore(i => i.PromoCodes)
            .Ignore(i => i.Id);

        config.NewConfig<UpdateCustomerRequest, Customer>()
            .Map(
                i => i.CustomerPreferences,
                i => i.PreferenceIds != null
                    ? i.PreferenceIds.Select(i => new CustomerPreference { PreferenceId = i }).ToList()
                    : null)
            .Ignore(i => i.PromoCodes)
            .Ignore(i => i.Id);

        config.NewConfig<Customer, CustomerResponse>()
            .Map(i => i.Preferences, i => i.CustomerPreferences.Select(i => i.Preference).ToList())
            .Map(i => i.PromoCodes, i => i.PromoCodes);

        config.NewConfig<Customer, CustomerShortResponse>();

        config.NewConfig<PromoCode, PromoCodeShortResponse>();

        config.NewConfig<Preference, PreferenceShortResponse>();

        config.NewConfig<Preference, PreferenceResponse>()
            .Map(i => i.Customers, i => i.CustomerPreferences.Select(i => i.Customer).ToList())
            .Map(i => i.PromoCodes, i => i.PromoCodes);

        config.NewConfig<PromoCode, PromoCodeShortResponse>();

        config.NewConfig<GivePromoCodeRequest, PromoCode>()
            .Ignore(
                i => i.Id, 
                i => i.PartnerManagerId, 
                i => i.PartnerManager,
                i => i.Preference,
                i => i.CustomerId,
                i => i.Customer,
                i => i.BeginDate, 
                i => i.EndDate)
            .Map(i => i.Code, i => i.PromoCode)
            .IgnoreIf((from, to) => string.IsNullOrEmpty(from.Preference), i => i.PreferenceId)
            .Map(
                i => i.PreferenceId, 
                i => MapContext.Current.GetService<IRepository<Preference>>().Items
                    .First(preference => preference.Name == i.Preference).Id)
            ;

        return config;
    }
}