﻿using MapsterMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Управление объектами 'Клиент'
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController
        : ControllerBase
    {
        private readonly IRepository<Customer> _repository;
        private readonly IMapper _mapper;

        public CustomersController(IRepository<Customer> repository, IMapper mapper)
        {
            _repository = repository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить всех клиентов
        /// </summary>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<CustomerShortResponse>>> GetCustomersAsync(CancellationToken cancellationToken)
        {
            var entities = await _repository.GetAllAsync(cancellationToken);

            var response = _mapper.Map<List<CustomerShortResponse>>(entities);

            return response;
        }

        /// <summary>
        /// Получить клиента по Id
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <response code="404"> Если объект не найден </response>
        [HttpGet("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id, CancellationToken cancellationToken)
        {
            var entity = await _repository.GetByIdAsync(id, cancellationToken);
            if (entity == null) return NotFound();

            var response = _mapper.Map<Customer, CustomerResponse>(entity);

            return response;
        }

        /// <summary>
        /// Создать клиента
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns> </returns>
        /// <response code="201"> Если объект создан </response>
        /// <response code="400"> Если объект не создан </response>
        [ProducesResponseType(StatusCodes.Status201Created)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateCustomerRequest request, CancellationToken cancellationToken)
        {
            Guid guid = await _repository.CreateAsync(
                _mapper.Map<CreateCustomerRequest, Customer>(request), cancellationToken);
            if (guid != default) return Created();

            return BadRequest();
        }

        /// <summary>
        /// Редактировать клиента
        /// </summary>
        /// <param name="request"></param>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns> </returns>
        /// <response code="404"> Если объект не найден </response>
        /// <response code="400"> Если объект не обновлен </response>
        /// <response code="200"> Если объект обновлен </response>
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status200OK)]
        public async Task<IActionResult> EditCustomersAsync(Guid id, UpdateCustomerRequest request, CancellationToken cancellationToken)
        {
            if (_repository.GetByIdAsync(id, cancellationToken) is null) return NotFound();

            var entity = _mapper.Map<UpdateCustomerRequest, Customer>(request);
            entity.Id = id;

            bool result = await _repository.UpdateAsync(entity, cancellationToken);
            if (result) return Ok();

            return BadRequest();
        }

        /// <summary>
        /// Удалить клиента
        /// </summary>
        /// <param name="id"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        /// <response code="404"> Если объект не найден </response>
        /// <response code="400"> Если объект не удален </response>
        /// <response code="200"> Если объект удален </response>
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        public async Task<IActionResult> DeleteCustomer(Guid id, CancellationToken cancellationToken)
        {
            if (await _repository.GetByIdAsync(id, cancellationToken) is null)
                return NotFound();

            bool result = await _repository.DeleteAsync(id, cancellationToken);
            if (result) return NoContent();

            return BadRequest();
        }
    }
}