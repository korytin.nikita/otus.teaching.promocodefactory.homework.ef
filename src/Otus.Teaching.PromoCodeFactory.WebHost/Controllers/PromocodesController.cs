﻿using MapsterMapper;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Управление объектами 'Предпочтения'
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController
        : ControllerBase
    {
        private readonly IRepository<PromoCode> _repository;
        private readonly IRepository<Customer> _customerRepository;
        private readonly IMapper _mapper;

        public PromocodesController(
            IRepository<PromoCode> repository, IRepository<Customer> customerRepository, IMapper mapper)
        {
            _repository = repository;
            _customerRepository = customerRepository;
            _mapper = mapper;
        }

        /// <summary>
        /// Получить все промокоды
        /// <param name="cancellationToken"></param>
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<ActionResult<List<PromoCodeShortResponse>>> GetPromocodesAsync(CancellationToken cancellationToken)
        {
            var entities = await _repository.GetAllAsync(cancellationToken);

            var response = _mapper.Map<List<PromoCodeShortResponse>>(entities);

            return response;
        }

        /// <summary>
        /// Создать промокод и выдать его клиентам с указанным предпочтением
        /// </summary>
        /// <param name="request"></param>
        /// <param name="cancellationToken"></param>
        /// <returns> </returns>
        /// <response code="200"> Если промокод выдан </response>
        /// <response code="400"> Если промокод не выдан или не найден пользователь с указанным предпочтением </response>
        [HttpPost]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<IActionResult> GivePromoCodesToCustomersWithPreferenceAsync(GivePromoCodeRequest request, CancellationToken cancellationToken)
        {
            Customer? targetCustomer = await _customerRepository.Items
                .FirstOrDefaultAsync(i => i.CustomerPreferences.Any(i => i.Preference.Name == request.Preference), cancellationToken);
            if (targetCustomer is null) return BadRequest();

            PromoCode promoCode = _mapper.Map<GivePromoCodeRequest, PromoCode>(request);
            promoCode.CustomerId = targetCustomer.Id;
            Guid guid = await _repository.CreateAsync(promoCode, cancellationToken);
                
            if (guid != default) return Ok();

            return BadRequest();
        }
    }
}