﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PreferenceResponse
    {
        public string? Name { get; set; }

        public ICollection<PromoCodeShortResponse>? PromoCodes { get; set; }

        public ICollection<CustomerShortResponse>? Customers { get; set; }
    }
}