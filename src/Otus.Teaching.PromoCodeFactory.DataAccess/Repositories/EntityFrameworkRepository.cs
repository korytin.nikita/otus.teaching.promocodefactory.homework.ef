﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class ProjectRepository : EntityFrameworkRepository<Customer>
    {
        public ProjectRepository(EntityFrameworkDatabaseContext dbContext) : base(dbContext)
        {
        }

        // stub: Overriding for correct adding children field 'CustomerPreferences' with Many-To-Many relationship
        public async override Task<bool> UpdateAsync(Customer item, CancellationToken cancelationToken = default)
        {
            ArgumentNullException.ThrowIfNull(item);

            var customerId = item.Id;
            var preferenceIds = item.CustomerPreferences.Select(i => i.PreferenceId).ToList();

            var toAdd = preferenceIds.Select(i => new CustomerPreference { CustomerId = customerId, PreferenceId = i }).ToList();
            var toDelete = DbContext.CustomerPreferences.Where(x => !preferenceIds.Contains(x.PreferenceId) && x.CustomerId == customerId).ToList();

            DbContext.CustomerPreferences.RemoveRange(toDelete);
            DbContext.CustomerPreferences.AddRange(toAdd);

            DbContext.Entry(item).State = EntityState.Modified;

            if (AutoSaveChanges)
                await DbContext.SaveChangesAsync(cancelationToken);

            return true;
        }
        // stub: Overriding for correct adding children field 'CustomerPreferences' with Many-To-Many relationship
        public override bool Update(Customer item)
        {
            ArgumentNullException.ThrowIfNull(item);

            var customerId = item.Id;
            var preferenceIds = item.CustomerPreferences.Select(i => i.PreferenceId).ToList();

            var toAdd = preferenceIds.Select(i => new CustomerPreference { CustomerId = customerId, PreferenceId = i }).ToList();
            var toDelete = DbContext.CustomerPreferences.Where(x => !preferenceIds.Contains(x.PreferenceId) && x.CustomerId == customerId).ToList();

            DbContext.CustomerPreferences.RemoveRange(toDelete);
            DbContext.CustomerPreferences.AddRange(toAdd);

            DbContext.Entry(item).State = EntityState.Modified;

            if (AutoSaveChanges)
                DbContext.SaveChanges();

            return true;
        }
    }

    public class EntityFrameworkRepository<T> : IRepository<T> where T : BaseEntity, new()
    {
        protected readonly EntityFrameworkDatabaseContext DbContext;
        protected DbSet<T> Set => DbContext.Set<T>();

        public bool AutoSaveChanges { get; set; } = true;
        public virtual IQueryable<T> Items => Set;

        public EntityFrameworkRepository(EntityFrameworkDatabaseContext dbContext)
        {
            DbContext = dbContext;
        }

        public virtual IEnumerable<T> GetAll() => Set
            .AsNoTracking().ToList();
        public virtual async Task<IEnumerable<T>> GetAllAsync(CancellationToken cancelationToken = default) => await Set
            .AsNoTracking().ToListAsync(cancelationToken);


        public virtual T? GetById(Guid id) => Set
            .AsNoTracking()
            .SingleOrDefault(item => item.Id == id);
        public virtual async Task<T?> GetByIdAsync(Guid id, CancellationToken cancelationToken = default) => await Set
            .AsNoTracking()
            .SingleOrDefaultAsync(item => item.Id == id, cancelationToken)
           ;

        public virtual Guid Create(T item)
        {
            ArgumentNullException.ThrowIfNull(item);

            DbContext.Add(item);
            if (AutoSaveChanges)
                DbContext.SaveChanges();

            return item.Id;
        }
        public virtual async Task<Guid> CreateAsync(T item, CancellationToken cancelationToken = default)
        {
            ArgumentNullException.ThrowIfNull(item);

            await DbContext.AddAsync(item, cancelationToken);
            if (AutoSaveChanges)
                await DbContext.SaveChangesAsync(cancelationToken);

            return item.Id;
        }

        public virtual bool Update(T item)
        {
            ArgumentNullException.ThrowIfNull(item);

            DbContext.Update(item);

            if (AutoSaveChanges)
                DbContext.SaveChanges();

            return true;
        }
        public virtual async Task<bool> UpdateAsync(T item, CancellationToken cancelationToken = default)
        {
            ArgumentNullException.ThrowIfNull(item);

            DbContext.Update(item);

            if (AutoSaveChanges)
                await DbContext.SaveChangesAsync(cancelationToken);

            return true;
        }

        public virtual bool Delete(Guid id)
        {
            var item = Set.AsNoTracking().First(i => i.Id == id);

            DbContext.Remove(item);

            if (AutoSaveChanges)
                DbContext.SaveChanges();

            return true;
        }
        public virtual async Task<bool> DeleteAsync(Guid id, CancellationToken cancelationToken = default)
        {
            var item = await Set.AsNoTracking().FirstAsync(i => i.Id == id, cancelationToken);

            DbContext.Remove(item);

            if (AutoSaveChanges)
                await DbContext.SaveChangesAsync(cancelationToken);

            return true;
        }

        public virtual bool SaveChanges()
        {
            DbContext.SaveChanges();

            return true;
        }
    }
}
