﻿using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Abstractions
{
    public interface IDatabaseInitializer
    {
        Task InitializeAsync();
        void Initialize();
    }
}
