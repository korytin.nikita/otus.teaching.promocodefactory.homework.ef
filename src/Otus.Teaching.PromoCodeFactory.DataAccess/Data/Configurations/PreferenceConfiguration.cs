﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data.Configurations
{
    public class PreferenceConfiguration : IEntityTypeConfiguration<Preference>
    {
        public void Configure(EntityTypeBuilder<Preference> modelBuilder)
        {
            modelBuilder
                .HasIndex(b => b.Name)
                .IsUnique();

            modelBuilder.Property(i => i.Name)
                .HasMaxLength(128);
        }
    }
}
