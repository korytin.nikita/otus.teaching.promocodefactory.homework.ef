﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data.Configurations
{
    public class CustomerPreferenceConfiguration : IEntityTypeConfiguration<CustomerPreference>
    {
        public void Configure(EntityTypeBuilder<CustomerPreference> modelBuilder)
        {

            modelBuilder
                .HasOne(i => i.Preference)
                .WithMany(i => i.CustomerPreferences)
                .HasForeignKey(i => i.PreferenceId);

            modelBuilder
                .HasOne(i => i.Customer)
                .WithMany(i => i.CustomerPreferences)
                .HasForeignKey(i => i.CustomerId);
        }
    }
}
