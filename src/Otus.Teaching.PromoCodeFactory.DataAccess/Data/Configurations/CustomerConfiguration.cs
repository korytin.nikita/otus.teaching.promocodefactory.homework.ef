﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data.Configurations
{
    public class CustomerConfiguration : IEntityTypeConfiguration<Customer>
    {
        public void Configure(EntityTypeBuilder<Customer> modelBuilder)
        {
            modelBuilder.Property(i => i.Email)
                .HasMaxLength(256);

            modelBuilder.Property(i => i.FirstName)
                .HasMaxLength(128);

            modelBuilder.Property(i => i.LastName)
                .HasMaxLength(128);
        }
    }
}
