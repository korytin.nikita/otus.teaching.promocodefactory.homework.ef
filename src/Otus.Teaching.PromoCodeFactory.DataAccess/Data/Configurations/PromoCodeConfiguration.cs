﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data.Configurations
{
    public class PromoCodeConfiguration : IEntityTypeConfiguration<PromoCode>
    {
        public void Configure(EntityTypeBuilder<PromoCode> modelBuilder)
        {
            modelBuilder
                .HasOne(i => i.Preference)
                .WithMany(i => i.PromoCodes)
                .HasForeignKey(i => i.PreferenceId);

            modelBuilder
               .HasOne(i => i.Customer)
               .WithMany(i => i.PromoCodes)
               .HasForeignKey(i => i.CustomerId);

            modelBuilder.Property(i => i.Code)
                .HasMaxLength(256);

            modelBuilder.Property(i => i.ServiceInfo)
                .HasMaxLength(256);

            modelBuilder.Property(i => i.PartnerName)
                .HasMaxLength(256);
        }
    }
}
