﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data.Configurations
{
    public class EmployeeConfiguration : IEntityTypeConfiguration<Employee>
    {
        public void Configure(EntityTypeBuilder<Employee> modelBuilder)
        {
            modelBuilder
                .HasOne(i => i.Role)
                .WithMany(i => i.Employees)
                .HasForeignKey(i => i.RoleId);

            modelBuilder.Property(i => i.FirstName)
               .HasMaxLength(128);

            modelBuilder.Property(i => i.LastName)
               .HasMaxLength(128);

            modelBuilder.Property(i => i.Email)
               .HasMaxLength(256);
        }
    } // todo move configurations
}
