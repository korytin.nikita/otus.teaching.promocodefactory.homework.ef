﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Otus.Teaching.PromoCodeFactory.DataAccess.Abstractions;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Data
{
    public class FakeDataOptions
    {
        public bool IsActive { get; set; }
    }
    public class FakeDataInitializer : IDatabaseInitializer
    {
        private readonly FakeDataOptions _options;

        public FakeDataInitializer(EntityFrameworkDatabaseContext dbContext, IOptions<FakeDataOptions> options, ILogger<FakeDataInitializer> Logger)
        {
            _dbContext = dbContext;
            _logger = Logger;
            _options = options.Value;
        }

        private readonly EntityFrameworkDatabaseContext _dbContext;
        private readonly ILogger _logger;

        public void Initialize()
        {
            throw new System.NotImplementedException();
        }

        public async Task InitializeAsync()
        {
            var timer = Stopwatch.StartNew();
            _logger.LogInformation("Инициализация БД...");

            //_logger.LogInformation("Удаление существующей БД...");
            //await _dbContext.Database.EnsureDeletedAsync();
            //_logger.LogInformation("Удаление существующей БД выполнено за {0} мс", timer.ElapsedMilliseconds);

            //_logger.LogInformation("Создание БД...");
            //_dbContext.Database.EnsureCreated();
            //_logger.LogInformation("Создание БД выполнено за {0} мс", timer.ElapsedMilliseconds);

            _logger.LogInformation("Миграция БД...");
            await _dbContext.Database.MigrateAsync();
            _logger.LogInformation("Миграция БД выполнена за {0} мс", timer.ElapsedMilliseconds);

            timer.Stop();

            timer = Stopwatch.StartNew();

            if (!_options.IsActive) return;

            _logger.LogInformation("Инициализация данных...");


            if (FakeDataFactory.CustomerPreferences != null && FakeDataFactory.CustomerPreferences.Any())
                await _dbContext.CustomerPreferences.AddRangeAsync(FakeDataFactory.CustomerPreferences);

            if (FakeDataFactory.Roles != null && FakeDataFactory.Roles.Any())
                await _dbContext.Roles.AddRangeAsync(FakeDataFactory.Roles);

            if (FakeDataFactory.Employees != null && FakeDataFactory.Employees.Any())
                await _dbContext.Employees.AddRangeAsync(FakeDataFactory.Employees);

            if (FakeDataFactory.Preferences != null && FakeDataFactory.Preferences.Any())
                await _dbContext.Preferences.AddRangeAsync(FakeDataFactory.Preferences);

            if (FakeDataFactory.Customers != null && FakeDataFactory.Customers.Any())
                await _dbContext.Customers.AddRangeAsync(FakeDataFactory.Customers);

            await _dbContext.SaveChangesAsync();

            _logger.LogInformation("Инициализация выполнена за {0} мс", timer.ElapsedMilliseconds);
        }
    }
}
