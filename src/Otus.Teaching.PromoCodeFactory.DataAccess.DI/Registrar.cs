﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Otus.Teaching.PromoCodeFactory.DataAccess.Data;
using Otus.Teaching.PromoCodeFactory.DataAccess.Abstractions;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.DataAccess.Repositories;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.DI
{
    public static class Registrar
    {
        public static IServiceCollection AddDatabase(this IServiceCollection services, IConfiguration configuration)
        {
            services
                .AddDbContext<EntityFrameworkDatabaseContext>(opt =>
                {
                    opt.UseLazyLoadingProxies();

                    string? type = configuration["Type"] 
                        ?? throw new DbContextConfiguratingException("Configuration not found");
                    string? connectionString = configuration.GetConnectionString(type) 
                        ?? throw new DbContextConfiguratingException("Configuration not found");
                    switch (type)
                    {
                        case null: throw new DbContextConfiguratingException("Database type not defined");
                        default: throw new DbContextConfiguratingException($"Connection type not supported. Type: {type}.");
                        case "LocalSQLite":
                            opt.UseSqlite(connectionString);
                            break;
                    }

                    opt.UseSnakeCaseNamingConvention();

                });

            if (configuration["Initialize"] == "true")
                services.AddTransient<IDatabaseInitializer, FakeDataInitializer>();

            return services;
            ;
        }

        public static IServiceCollection AddRepositories(this IServiceCollection services) => services
           .AddScoped(typeof(IRepository<>), typeof(EntityFrameworkRepository<>));
    }
}
