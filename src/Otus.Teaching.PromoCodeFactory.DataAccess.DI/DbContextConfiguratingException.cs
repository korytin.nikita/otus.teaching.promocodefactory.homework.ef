﻿namespace Otus.Teaching.PromoCodeFactory.DataAccess.DI
{
    internal class DbContextConfiguratingException : Exception
    {
        public DbContextConfiguratingException(string message) : base(message) { }
    } 
}
